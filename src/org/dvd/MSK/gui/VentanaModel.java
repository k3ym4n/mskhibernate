package org.dvd.MSK.gui;

import org.dvd.MSK.base.*;
import org.dvd.MSK.util.HibernateUtil;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;



import java.util.ArrayList;
import java.util.List;

/**
 * Created by k3ym4n on 02/03/2016.
 */
public class VentanaModel {




    public VentanaModel(){
        conectar();

    }

    private void conectar(){
        try{
            HibernateUtil.buildSessionFactory();
            HibernateUtil.openSession();
        }catch(HibernateException he){
            he.printStackTrace();
        }

    }

    public void desconectar(){
        try{
            HibernateUtil.closeSessionFactory();
        }catch(HibernateException he){
            he.printStackTrace();
        }

    }


    public ArrayList<Grupo> getListaGrupos(){
       Query query = HibernateUtil.getCurrentSession().createQuery("FROM Grupo");
        ArrayList<Grupo> grupos = (ArrayList<Grupo>) query.list();
        return  grupos;

    }

    public ArrayList<Disco> getListaDiscos(){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Disco");
        ArrayList<Disco> discos = (ArrayList<Disco>) query.list();
        return discos;
    }

    public ArrayList<Cancion>  getListaCanciones(){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Cancion");
        ArrayList<Cancion> canciones = (ArrayList<Cancion>) query.list();
        return canciones;
    }



    public ArrayList<Concierto> getListaConciertos(){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Concierto");
        ArrayList<Concierto> conciertos = (ArrayList<Concierto>) query.list();
        return conciertos;
    }

    public void guardarHibernate(Object objeto){
        Session session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.save(objeto);
        session.getTransaction().commit();
        session.close();
    }

    public void modificarHibernate(Object objeto){
        Session session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.update(objeto);
        session.getTransaction().commit();
        session.close();
    }

    public void eliminarHibernate(Object objeto){
        Session session = HibernateUtil.getCurrentSession();
        session.beginTransaction();
        session.delete(objeto);
        session.getTransaction().commit();
        session.close();
    }

    public Grupo getGrupo(String nombre){
       Query query = HibernateUtil.getCurrentSession().createQuery("FROM Grupo WHERE nombre =:nombre");
        query.setParameter("nombre", nombre);
        Grupo grupo = (Grupo) query.uniqueResult();
        return  grupo;
    }


    public Disco getDisco(String titulo) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Disco WHERE titulo =:titulo");
        query.setParameter("titulo", titulo);
        Disco disco = (Disco) query.uniqueResult();
        return  disco;
    }
    public Cancion getCancion(String titulo) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Cancion WHERE titulo =:titulo");
        query.setParameter("titulo", titulo);
        Cancion cancion = (Cancion) query.uniqueResult();
        return  cancion;
    }

    public Concierto getConcierto(String nombre){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Concierto WHERE nombre =:nombre");
        query.setParameter("nombre",nombre);
        Concierto concierto = (Concierto) query.uniqueResult();
        return concierto;
    }


    public ArrayList<Grupo>  getBuscarGrupo(String nombre){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Grupo WHERE nombre =:nombre");
        query.setParameter("nombre", nombre);
        ArrayList<Grupo> grupo = (ArrayList<Grupo>) query.list();
        return  grupo;
    }

    public ArrayList<Disco> getBuscarDisco(String titulo) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Disco WHERE titulo =:titulo");
        query.setParameter("titulo", titulo);
        ArrayList<Disco> disco = (ArrayList<Disco>) query.list();
        return  disco;
    }
    public ArrayList<Cancion> getBuscarCancion(String titulo) {
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Cancion WHERE titulo =:titulo");
        query.setParameter("titulo", titulo);
        ArrayList<Cancion> cancion = (ArrayList<Cancion>) query.list();
        return  cancion;
    }

    public ArrayList<Concierto> getBuscarConcierto(String nombre){
        Query query = HibernateUtil.getCurrentSession().createQuery("FROM Concierto WHERE nombre =:nombre");
        query.setParameter("nombre",nombre);
        ArrayList<Concierto> concierto = (ArrayList<Concierto>) query.list();
        return concierto;
    }
}














































