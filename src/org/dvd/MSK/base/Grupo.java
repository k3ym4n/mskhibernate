package org.dvd.MSK.base;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by k3ym4n on 02/03/2016.
 */
@Entity
@Table(name = "Grupo")
public class Grupo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "genero")
    private String genero;
    @Column(name = "fecha_inicio")
    private Date fecha_inicio;
    @Column(name = "fecha_fin")
    private Date fecha_fin;
    @Column(name = "nacionalidad")
    private String nacionalidad;

    @OneToMany(mappedBy = "grupo" ,cascade = CascadeType.DETACH)
    List<Disco> disco;

    @OneToMany(mappedBy = "grupo" ,cascade = CascadeType.ALL)
    List<Recopilatorio> recopilatorio;

    @ManyToMany(cascade = CascadeType.DETACH,mappedBy = "grupo")
    List<Concierto> concierto;

    public Grupo(){
        disco = new ArrayList<>();
        recopilatorio = new ArrayList<>();
        concierto = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public Date getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(Date fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public Date getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_fin(Date fecha_fin) {
        this.fecha_fin = fecha_fin;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public List<Disco> getDisco() {
        return disco;
    }

    public void setDisco(List<Disco> disco) {
        this.disco = disco;
    }

    public List<Recopilatorio> getRecopilatorio() {
        return recopilatorio;
    }

    public void setRecopilatorio(List<Recopilatorio> recopilatorio) {
        this.recopilatorio = recopilatorio;
    }

    public List<Concierto> getConcierto() {
        return concierto;
    }

    public void setConcierto(List<Concierto> concierto) {
        this.concierto = concierto;
    }

    @Override
    public String toString() {
        return  nombre ;
    }
}
