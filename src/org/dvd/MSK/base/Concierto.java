package org.dvd.MSK.base;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by k3ym4n on 02/03/2016.
 */
@Entity
@Table(name = "Concierto")
public class Concierto  {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "fecha")
    private Date fecha;
    @Column(name = "precio")
    private float precio;

    @ManyToMany(cascade = CascadeType.DETACH)
    @JoinTable(name = "grupo_concierto",joinColumns = {@JoinColumn(name = "id_concierto")},inverseJoinColumns = {@JoinColumn(name = "id_grupo")})
    List<Grupo> grupo;

    @ManyToMany(cascade = CascadeType.DETACH)
    @JoinTable(name = "cancion_concierto",joinColumns = {@JoinColumn(name = "id_concierto")},inverseJoinColumns = {@JoinColumn(name = "id_cancion")})
    List<Cancion> cancion;

    public Concierto(){
        grupo = new ArrayList<>();
        cancion = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public List<Grupo> getGrupo() {
        return grupo;
    }

    public void setGrupo(List<Grupo> grupo) {
        this.grupo = grupo;
    }

    public List<Cancion> getCancion() {
        return cancion;
    }

    public void setCancion(List<Cancion> cancion) {
        this.cancion = cancion;
    }

    @Override
    public String toString() {
        return nombre;
    }
}
