package org.dvd.MSK.gui;

import org.dvd.MSK.base.*;
import org.dvd.MSK.face.Botonera;
import org.dvd.MSK.face.Ventana;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by k3ym4n on 02/03/2016.
 */
public class VentanaController implements ActionListener {

    private Ventana view;
    private VentanaModel model;
    private Botonera botonera;


    private DefaultTableModel dtmGrupo, dtmDisco, dtmCancion, dtmGira, dtmConcierto;
    private DefaultListModel dlmGrupo, dlmConcierto, dlmGira;

    private Grupo grupo;
    private Disco disco;
    private Cancion cancion;
    //private Recopilatorio recopilatorio;
    private Concierto concierto;

    public ArrayList<Grupo> listaGrupos;
    public ArrayList<Disco> listaDiscos;
    public ArrayList<Cancion> listaCanciones;
    public ArrayList<Concierto> listaConciertos;

    public VentanaController(Ventana view, VentanaModel model, Botonera botonera) {

        this.view = view;
        this.model = model;
        this.botonera = botonera;

        listaGrupos = new ArrayList<>();
        listaDiscos = new ArrayList<>();
        listaConciertos = new ArrayList<>();
        listaCanciones = new ArrayList<>();

        dlmGrupo = new DefaultListModel();
        dlmConcierto = new DefaultListModel();
        dlmGira = new DefaultListModel();
        view.JlistaGrupos.setModel(dlmGrupo);
        view.JlistaConciertos.setModel(dlmConcierto);
        view.JlistaGira.setModel(dlmGira);

        modeloListaGrupos();
        modeloListaConciertos();

       // modeloListaGira();
        limpiarTF();
        botoneraInicio();
        listenerBotones();

        modelosTablas();

        listarTablaGrupos();
        listarTablaDiscos();
        listarTablaCanciones();
        listarTablaConciertos();


        //  listarTablaRecopilatorios();

        rellenarCombodiscoGrupo();
        rellenarCombocancionDisco();
        //rellenarComboGiraGrupo();
        rellenarComboGiraConcierto();
        //rellenarCBConciertoGrupo();
        //rellenarCBConciertoCancion();

        boolean D = false;
        edicionCamposInicio(D);
        view.PanelPrincipal.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                int pestana = view.PanelPrincipal.getSelectedIndex();
                switch (pestana) {
                    case 0:

                        limpiarTF();
                        botoneraInicio();
                        rellenarCombodiscoGrupo();
                        rellenarCombocancionDisco();
                        edicionCamposInicio(D);
                        view.tablaDgrupos.addMouseListener(new MouseAdapter() {
                            @Override
                            public void mouseClicked(MouseEvent e) {
                                int filaG = view.tablaDgrupos.rowAtPoint(e.getPoint());
                                view.tfgruponombre.setText(view.tablaDgrupos.getValueAt(filaG, 0).toString());
                                view.tfgrupogenero.setText(view.tablaDgrupos.getValueAt(filaG, 1).toString());
                                view.dategrupofinicio.setDate((Date) view.tablaDgrupos.getValueAt(filaG, 2));
                                view.dategrupoffin.setDate((Date) view.tablaDgrupos.getValueAt(filaG, 3));
                                view.tfgruponacionalidad.setText(view.tablaDgrupos.getValueAt(filaG, 4).toString());
                                botoneraListas();
                                edicionCamposInicio(true);
                            }
                        });
                        break;
                    case 1:
                        limpiarTF();
                        botoneraInicio();
                        rellenarCombodiscoGrupo();
                        rellenarCombocancionDisco();
                        edicionCamposInicio(D);
                        view.tablaDdiscos.addMouseListener(new MouseAdapter() {
                            @Override
                            public void mouseClicked(MouseEvent e) {
                                Disco disco = model.getDisco((String) view.tablaDdiscos.getValueAt(view.tablaDdiscos.getSelectedRow(), 0));
                                view.tfdiscotitulo.setText(disco.getTitulo());
                                view.floatdiscoduracion.setText(String.valueOf(disco.getDuracion()));
                                view.tfdiscoproductora.setText(disco.getProductora());
                                view.intdisconumcanciones.setText(String.valueOf(disco.getNum_canciones()));
                                view.floatdiscoprecio.setText(String.valueOf(disco.getPrecio()));
                                view.cbdiscoGrupo.setSelectedItem(disco.getGrupo());
                                botoneraListas();
                                edicionCamposInicio(true);
                            }
                        });
                        break;
                    case 2:
                        limpiarTF();
                        botoneraInicio();
                        rellenarCombodiscoGrupo();
                        rellenarCombocancionDisco();
                        edicionCamposInicio(D);
                        view.tablaDcanciones.addMouseListener(new MouseAdapter() {
                            @Override
                            public void mouseClicked(MouseEvent e) {
                                Cancion cancion = model.getCancion((String) view.tablaDcanciones.getValueAt(view.tablaDcanciones.getSelectedRow(), 0));
                                view.tfcanciontitulo.setText(cancion.getTitulo());
                                view.floatcancionduracion.setText(String.valueOf(cancion.getDuracion()));
                                view.intcanciontrack.setText(String.valueOf(cancion.getTrack()));
                                view.floatcancionprecio.setText(String.valueOf(cancion.getPrecio()));
                                view.tfcanciongenero.setText(cancion.getGenero());
                                view.cbcancionDisco.setSelectedItem(cancion.getDisco());
                                botoneraListas();
                                edicionCamposInicio(true);
                            }
                        });
                        break;
                    case 3:
                        limpiarTF();
                        botoneraInicio();
                        rellenarCombodiscoGrupo();
                        rellenarCombocancionDisco();
                        edicionCamposInicio(D);

                        listarTablaConciertos();
                        view.JlistaConciertos.setSelectedIndex(-1);
                        view.tablaConciertos.addMouseListener(new MouseAdapter() {
                            @Override
                            public void mouseClicked(MouseEvent e) {
                                Concierto concierto = model.getConcierto((String) view.tablaConciertos.getValueAt(view.tablaConciertos.getSelectedRow(), 0));
                                view.tfconciertonombre.setText(concierto.getNombre());
                                view.dateconciertofecha.setDate(concierto.getFecha());
                                view.floatconciertoprecio.setText(String.valueOf(concierto.getPrecio()));

                                botoneraListas();
                                edicionCamposInicio(true);
                            }
                        });


                        modeloListaGrupos();

                        break;
                    case 4:
                        rellenarComboGiraConcierto();
                        modeloListaConciertos();
                        modeloListaGrupos();
                        modeloListaGira();
                        view.JlistaConciertos.addListSelectionListener(new ListSelectionListener() {
                            @Override
                            public void valueChanged(ListSelectionEvent e) {
                                Concierto concierto = (Concierto) view.JlistaConciertos.getSelectedValue();
                                dlmGira.removeAllElements();
                                dlmGira.addElement(concierto.getGrupo());

                            }

                        });

                        modeloListaGira();
                       /* view.JlistaGrupos.addListSelectionListener(new ListSelectionListener() {
                            @Override
                            public void valueChanged(ListSelectionEvent e) {
                                Grupo grupo = (Grupo) view.JlistaGrupos.getSelectedValue();
                                dlmGira.removeAllElements();

                                dlmGira.addElement(grupo.getConcierto());
                            }
                        });*/

                        break;
                }
            }
        });

    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////

    /**
     * Metodos para los listeners de la botonera
     */

    private void listenerBotones() {
        botonera.btnuevo.addActionListener(this);
        botonera.btguardar.addActionListener(this);
        botonera.btcancelar.addActionListener(this);
        botonera.bteliminar.addActionListener(this);
        botonera.btmodificar.addActionListener(this);

        view.btguardarGira.addActionListener(this);
        view.btbuscarGrupo.addActionListener(this);
        view.btbuscarCancion.addActionListener(this);
        view.btbuscarDisco.addActionListener(this);
        view.btbuscarConcierto.addActionListener(this);
    }

    private void botoneraInicio() {
        botonera.btnuevo.setEnabled(true);
        botonera.btguardar.setEnabled(false);
        botonera.btcancelar.setEnabled(false);
        botonera.bteliminar.setEnabled(false);
        botonera.btmodificar.setEnabled(false);
    }

    private void botoneraListas() {
        botonera.btnuevo.setEnabled(true);
        botonera.btguardar.setEnabled(false);
        botonera.btcancelar.setEnabled(false);
        botonera.bteliminar.setEnabled(true);
        botonera.btmodificar.setEnabled(true);
    }

    private void edicionCamposInicio(boolean D) {
        int pestana = view.PanelPrincipal.getSelectedIndex();
        switch (pestana) {
            case 0:
                view.tfgruponombre.setEditable(D);
                view.tfgrupogenero.setEditable(D);
                view.dategrupofinicio.setEnabled(D);
                view.dategrupoffin.setEnabled(D);
                view.tfgruponacionalidad.setEditable(D);
                break;
            case 1:
                view.tfdiscotitulo.setEditable(D);
                view.floatdiscoduracion.setEditable(D);
                view.tfdiscoproductora.setEditable(D);
                view.floatdiscoprecio.setEditable(D);
                view.intdisconumcanciones.setEditable(D);
                view.cbdiscoGrupo.setEnabled(D);
                break;
            case 2:
                view.tfcanciontitulo.setEditable(D);
                view.floatcancionduracion.setEditable(D);
                view.intcanciontrack.setEditable(D);
                view.floatcancionprecio.setEditable(D);
                view.tfcanciongenero.setEditable(D);
                view.cbcancionDisco.setEnabled(D);
                break;
            case 3:
                view.tfconciertonombre.setEditable(D);
                view.dateconciertofecha.setEnabled(D);
                view.floatconciertoprecio.setEditable(D);
                break;
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void actionPerformed(ActionEvent e) {
        int pestana = view.PanelPrincipal.getSelectedIndex();

        if (e.getSource() == botonera.btnuevo) {
            botonera.btnuevo.setEnabled(false);
            botonera.btguardar.setEnabled(true);
            botonera.btcancelar.setEnabled(true);
            botonera.bteliminar.setEnabled(false);
            botonera.btmodificar.setEnabled(true);
            modeloListaGira();
            switch (pestana) {
                case 0:
                    limpiarTF();
                    edicionCamposInicio(true);
                    break;
                case 1:
                    limpiarTF();
                    view.cbdiscoGrupo.setSelectedIndex(0);
                    edicionCamposInicio(true);
                    break;
                case 2:
                    limpiarTF();
                    view.cbcancionDisco.setSelectedIndex(0);
                    edicionCamposInicio(true);
                    break;
                case 3:
                    limpiarTF();
                    edicionCamposInicio(true);
                    break;
                case 4:
                    view.cbGiraGrupos.setSelectedIndex(0);
                    break;
                default:
                    break;
            }
        } else if (e.getSource() == botonera.btguardar) {
            botonera.btnuevo.setEnabled(true);
            botonera.btguardar.setEnabled(false);
            botonera.btcancelar.setEnabled(true);
            botonera.bteliminar.setEnabled(true);
            botonera.btmodificar.setEnabled(true);
            switch (pestana) {
                case 0:
                    guardarGrupo();
                    limpiarTF();
                    rellenarCombodiscoGrupo();
                    //rellenarComboGiraGrupo();
                    //rellenarCBConciertoGrupo();
                    modeloListaGrupos();
                    listarTablaGrupos();
                    break;
                case 1:
                    guardarDisco();
                    rellenarCombocancionDisco();
                    limpiarTF();
                    listarTablaDiscos();
                    break;
                case 2:
                    guardarCancion();
                    limpiarTF();
                    listarTablaCanciones();
                   // rellenarCBConciertoCancion();
                    rellenarComboGiraConcierto();
                    break;
                case 3:
                    guardarConcierto();
                    limpiarTF();
                    listarTablaConciertos();

                    //rellenarComboGiraGrupo();
                    break;
                default:
                    break;
            }
        } else if (e.getSource() == botonera.btmodificar) {
            botonera.btnuevo.setEnabled(false);
            botonera.btguardar.setEnabled(true);
            botonera.btcancelar.setEnabled(true);
            botonera.bteliminar.setEnabled(true);
            botonera.btmodificar.setEnabled(false);
            modeloListaGira();
            switch (pestana) {
                case 0:
                    int filaG = view.tablaDgrupos.getSelectedRow();
                    String nombre = (String) view.tablaDgrupos.getValueAt(filaG, 0);
                    grupo = model.getGrupo(nombre);
                    modificarGrupo(grupo);
                    //rellenarComboGiraGrupo();
                    rellenarCombodiscoGrupo();
                    modeloListaGrupos();
                    listarTablaGrupos();
                    break;
                case 1:
                    int filaD = view.tablaDdiscos.getSelectedRow();
                    String tituloD = (String) view.tablaDdiscos.getValueAt(filaD, 0);
                    disco = model.getDisco(tituloD);
                    modificarDisco(disco);
                    rellenarCombocancionDisco();

                    listarTablaDiscos();
                    break;
                case 2:
                    int filaC = view.tablaDcanciones.getSelectedRow();
                    String tituloC = (String) view.tablaDcanciones.getValueAt(filaC, 0);
                    cancion = model.getCancion(tituloC);
                    modificarCancion(cancion);
                    listarTablaCanciones();
                    break;
                case 3:
                    int filaCo = view.tablaConciertos.getSelectedRow();
                    String tituloCo = (String) view.tablaConciertos.getValueAt(filaCo, 0);
                    concierto = model.getConcierto(tituloCo);
                    modificarConcierto(concierto);
                    rellenarComboGiraConcierto();
                    listarTablaConciertos();
                    break;
                default:
                    break;
            }
        } else if (e.getSource() == botonera.bteliminar) {
            botonera.btnuevo.setEnabled(true);
            botonera.btguardar.setEnabled(true);
            botonera.btcancelar.setEnabled(true);
            botonera.bteliminar.setEnabled(true);
            botonera.btmodificar.setEnabled(true);
            modeloListaGira();
            switch (pestana) {
                case 0:
                    int filaG = view.tablaDgrupos.getSelectedRow();
                    String nombre = (String) view.tablaDgrupos.getValueAt(filaG, 0);

                    for(int i=0;i <model.getListaDiscos().size();i++){
                        if(nombre.equals(model.getListaDiscos().get(i).getGrupo().getNombre())){
                            JOptionPane.showMessageDialog(null,"WOW intentas borrar un grupo que esta siendo usado en discos, you are so Bad");
                            return;
                        }
                    }
                    grupo = model.getGrupo(nombre);
                    model.eliminarHibernate(grupo);
                    rellenarCombodiscoGrupo();
                   // rellenarComboGiraGrupo();
                    modeloListaGrupos();
                    listarTablaGrupos();
                    limpiarTF();
                    break;
                case 1:
                    int filaD = view.tablaDdiscos.getSelectedRow();
                    String tituloD = (String) view.tablaDdiscos.getValueAt(filaD, 0);

                    for(int i=0;i <model.getListaCanciones().size();i++){
                        if(tituloD.equals(model.getListaCanciones().get(i).getDisco().getTitulo())){
                            JOptionPane.showMessageDialog(null,"WOW intentas borrar un grupo que esta siendo usado en discos, you are so Bad");
                            return;
                        }
                    }

                    disco = model.getDisco(tituloD);
                    model.eliminarHibernate(disco);
                    rellenarCombocancionDisco();
                    listarTablaDiscos();
                    limpiarTF();
                    break;
                case 2:
                    int filaC = view.tablaDcanciones.getSelectedRow();
                    String tituloC = (String) view.tablaDcanciones.getValueAt(filaC, 0);
                    cancion = model.getCancion(tituloC);

                    model.eliminarHibernate(cancion);
                    listarTablaCanciones();
                    limpiarTF();
                    break;
                case 3:
                    int filaCo = view.tablaConciertos.getSelectedRow();
                    String nombreCo = (String) view.tablaConciertos.getValueAt(filaCo, 0);
                    concierto = model.getConcierto(nombreCo);

                    model.eliminarHibernate(concierto);
                    rellenarComboGiraConcierto();
                    listarTablaConciertos();
                    limpiarTF();
                    break;
                default:
                    break;
            }
        } else if (e.getSource() == botonera.btcancelar) {
            botonera.btnuevo.setEnabled(true);
            botonera.btguardar.setEnabled(true);
            botonera.btcancelar.setEnabled(true);
            botonera.bteliminar.setEnabled(true);
            botonera.btmodificar.setEnabled(true);
            modeloListaGira();
            switch (pestana) {
                case 0:
                    limpiarTF();
                    break;
                case 1:
                    limpiarTF();
                    break;
                case 2:
                    limpiarTF();
                    break;
                case 3:
                    limpiarTF();
                    break;
                default:
                    break;
            }
        } else if (e.getSource() == view.btguardarGira) {
            guardarGira();
            limpiarTF();
            modeloListaConciertos();
           // modeloListaGira();
            view.JlistaGrupos.setSelectedIndex(-1);


        } else if (e.getSource() == view.JlistaGrupos) {
            Grupo grupo = (Grupo) view.JlistaGrupos.getSelectedValue();
            dlmGira.removeAllElements();
            dlmGira.addElement(grupo.getConcierto());
        } else if (e.getSource() == view.btbuscarGrupo) {

            if (view.tfgrupobuscar.getText().equalsIgnoreCase("")) {
                listarTablaGrupos();
                return;
            }
            dtmGrupo.setNumRows(0);
            List<Grupo> listaG = model.getBuscarGrupo(view.tfgrupobuscar.getText());
            for (Grupo grupo : listaG) {
                Object[] filaG = new Object[]{grupo.getNombre(), grupo.getGenero(), grupo.getFecha_inicio(), grupo.getFecha_fin(), grupo.getNacionalidad()};
                dtmGrupo.addRow(filaG);
            }
        }else if (e.getSource() == view.btbuscarDisco) {

            if (view.tfbuscardisco.getText().equalsIgnoreCase("")) {
                listarTablaDiscos();
                return;
            }
            List<Disco> listaDiscos = model.getBuscarDisco(view.tfbuscardisco.getText());
            dtmDisco.setNumRows(0);

            for (Disco disco : listaDiscos) {
                Object[] filaD = new Object[]{disco.getTitulo(), disco.getDuracion(), disco.getProductora(), disco.getNum_canciones(), disco.getPrecio(), disco.getNombre_grupo()};
                dtmDisco.addRow(filaD);
            }
        }else if (e.getSource() == view.btbuscarCancion) {
            if (view.tfcancionbuscar.getText().equalsIgnoreCase("")) {
                listarTablaCanciones();
                return;
            }
            List<Cancion> listaCanciones = model.getBuscarCancion(view.tfcancionbuscar.getText());
            dtmCancion.setNumRows(0);

            for (Cancion cancion : listaCanciones) {
                Object[] filaC = new Object[]{cancion.getTitulo(), cancion.getDuracion(), cancion.getTrack(), cancion.getPrecio(), cancion.getGenero(), cancion.getTitulo_disco()};
                dtmCancion.addRow(filaC);
            }

        }else if (e.getSource() == view.btbuscarConcierto) {
            if (view.tfconciertobuscar.getText().equalsIgnoreCase("")) {
                listarTablaConciertos();
                return;
            }
            List<Concierto> listaConciertos = model.getBuscarConcierto(view.tfconciertobuscar.getText());
            dtmConcierto.setNumRows(0);
            for (Concierto concierto : listaConciertos) {
                Object[] filaCon = new Object[]{concierto.getNombre(), concierto.getFecha(), concierto.getPrecio()};
                dtmConcierto.addRow(filaCon);
            }
        }

    }

   /* private void rellenarComboGiraGrupo() {
        view.cbGiraGrupos.removeAllItems();
        ArrayList<Grupo> grupos = model.getListaGrupos();
        for (Grupo grupo : grupos) {
            view.cbGiraGrupos.addItem(grupo);
        }
    }*/

    private void rellenarComboGiraConcierto() {
        view.cbGiraConciertos.removeAllItems();
        ArrayList<Concierto> conciertos = model.getListaConciertos();
        for (Concierto concierto : conciertos) {
            view.cbGiraConciertos.addItem(concierto);
        }
    }


    /**
     * modelos de las tablas y de las listas
     */
    private void modelosTablas() {

        dtmGrupo = new DefaultTableModel();
        dtmGrupo.addColumn("Nombre");
        dtmGrupo.addColumn("Genero");
        dtmGrupo.addColumn("Fecha inicio");
        dtmGrupo.addColumn("Fecha fin");
        dtmGrupo.addColumn("Nacionalidad");
        view.tablaDgrupos.setModel(dtmGrupo);

        dtmDisco = new DefaultTableModel();
        dtmDisco.addColumn("Titulo");
        dtmDisco.addColumn("Duracion");
        dtmDisco.addColumn("Productora");
        dtmDisco.addColumn("Numero de Canciones");
        dtmDisco.addColumn("Precio");
        dtmDisco.addColumn("Grupo");
        view.tablaDdiscos.setModel(dtmDisco);

        dtmCancion = new DefaultTableModel();
        dtmCancion.addColumn("Titulo");
        dtmCancion.addColumn("Duracion");
        dtmCancion.addColumn("Track");
        dtmCancion.addColumn("Precio");
        dtmCancion.addColumn("Genero");
        dtmCancion.addColumn("Disco");
        view.tablaDcanciones.setModel(dtmCancion);

        dtmConcierto = new DefaultTableModel();
        dtmConcierto.addColumn("Nombre");
        dtmConcierto.addColumn("Fecha");
        dtmConcierto.addColumn("Precio");
        dtmConcierto.addColumn("Grupo");
        view.tablaConciertos.setModel(dtmConcierto);


    }

    /**
     * ----------------------------------------------------------------------------------------------------------------
     */
    private void limpiarTF() {
        int pestana = view.PanelPrincipal.getSelectedIndex();
        switch (pestana) {
            case 0:
                view.tfgruponombre.setText("");
                view.tfgrupogenero.setText("");
                view.dategrupofinicio.setDate(null);
                view.dategrupoffin.setDate(null);
                view.tfgruponacionalidad.setText("");
                break;
            case 1:
                view.tfdiscotitulo.setText("");
                view.floatdiscoduracion.setText("");
                view.tfdiscoproductora.setText("");
                view.floatdiscoprecio.setText("");
                view.intdisconumcanciones.setText("");
                view.cbdiscoGrupo.setSelectedItem("Selecciona un grupo");
                break;
            case 2:
                view.tfcanciontitulo.setText("");
                view.floatcancionduracion.setText("");
                view.intcanciontrack.setText("");
                view.floatcancionprecio.setText("");
                view.tfcanciongenero.setText("");
                view.cbcancionDisco.setSelectedItem("Selecciona un disco");
                break;
            case 3:
                view.tfconciertonombre.setText("");
                view.dateconciertofecha.setDate(null);
                view.floatconciertoprecio.setText("");
                break;
        }
    }


    private void listarTablaGrupos() {
        List<Grupo> listaGrupos = model.getListaGrupos();
        dtmGrupo.setNumRows(0);

        for (Grupo grupo : listaGrupos) {
            Object[] filaG = new Object[]{grupo.getNombre(), grupo.getGenero(), grupo.getFecha_inicio(), grupo.getFecha_fin(), grupo.getNacionalidad()};
            dtmGrupo.addRow(filaG);
        }
    }

    private void listarTablaDiscos() {
        List<Disco> listaDiscos = model.getListaDiscos();
        dtmDisco.setNumRows(0);

        for (Disco disco : listaDiscos) {
            Object[] filaD = new Object[]{disco.getTitulo(), disco.getDuracion(), disco.getProductora(), disco.getNum_canciones(), disco.getPrecio(), disco.getNombre_grupo()};
            dtmDisco.addRow(filaD);
        }
    }

    private void listarTablaCanciones() {
        List<Cancion> listaCanciones = model.getListaCanciones();
        dtmCancion.setNumRows(0);

        for (Cancion cancion : listaCanciones) {
            Object[] filaC = new Object[]{cancion.getTitulo(), cancion.getDuracion(), cancion.getTrack(), cancion.getPrecio(), cancion.getGenero(), cancion.getTitulo_disco()};
            dtmCancion.addRow(filaC);
        }
    }

    private void listarTablaConciertos() {
        List<Concierto> listaConciertos = model.getListaConciertos();
        dtmConcierto.setNumRows(0);
        for (Concierto concierto : listaConciertos) {
            Object[] filaCon = new Object[]{concierto.getNombre(), concierto.getFecha(), concierto.getPrecio(),concierto.getGrupo(),concierto.getCancion()};
            dtmConcierto.addRow(filaCon);
        }


    }


    private void modeloListaGrupos() {
        List<Grupo> listaGrupos = model.getListaGrupos();
        dlmGrupo.removeAllElements();
        for (Grupo grupo : listaGrupos) {
            dlmGrupo.addElement(grupo);
        }
    }

    private void modeloListaConciertos() {
        List<Concierto> listaConciertos = model.getListaConciertos();
        dlmConcierto.removeAllElements();
        for (Concierto concierto : listaConciertos) {
            dlmConcierto.addElement(concierto);
        }
    }

    private void modeloListaGira() {
        List<Concierto> listaDConciertos = model.getListaConciertos();
        dlmGira.removeAllElements();
        for (Concierto concierto : listaDConciertos) {
            dlmGira.addElement(concierto.getGrupo());
        }

    }


    private void guardarGrupo() {
        grupo = new Grupo();
        grupo.setNombre(view.tfgruponombre.getText());
        grupo.setGenero(view.tfgrupogenero.getText());
        grupo.setFecha_inicio(view.dategrupofinicio.getDate());
        grupo.setFecha_fin(view.dategrupoffin.getDate());
        grupo.setNacionalidad(view.tfgruponacionalidad.getText());

        listaGrupos.add(grupo);
        model.guardarHibernate(grupo);
    }

    private void guardarDisco() {
        disco = new Disco();
        disco.setTitulo(view.tfdiscotitulo.getText());
        disco.setDuracion(Float.parseFloat(view.floatdiscoduracion.getText()));
        disco.setProductora(view.tfdiscoproductora.getText());
        disco.setNum_canciones(Integer.parseInt(view.intdisconumcanciones.getText()));
        disco.setPrecio(Float.parseFloat(view.floatdiscoprecio.getText()));
        disco.setNombre_grupo(view.cbdiscoGrupo.getSelectedItem().toString());
        disco.setGrupo((Grupo) view.cbdiscoGrupo.getSelectedItem());
        listaDiscos.add(disco);
        model.guardarHibernate(disco);
    }

    private void guardarCancion() {
        cancion = new Cancion();
        cancion.setTitulo(view.tfcanciontitulo.getText());
        cancion.setDuracion(Float.parseFloat(view.floatcancionduracion.getText()));
        cancion.setTrack(Integer.parseInt(view.intcanciontrack.getText()));
        cancion.setPrecio(Float.parseFloat(view.floatcancionprecio.getText()));
        cancion.setGenero(view.tfcanciongenero.getText());
        cancion.setTitulo_disco(view.cbcancionDisco.getSelectedItem().toString());
        cancion.setDisco((Disco) view.cbcancionDisco.getSelectedItem());
        listaCanciones.add(cancion);
        model.guardarHibernate(cancion);
    }

   /* private void rellenarCBConciertoGrupo(){
        view.cbConciertoGrupo.removeAllItems();
        ArrayList<Grupo> grupos = model.getListaGrupos();
        for (Grupo grupo : grupos) {
            view.cbConciertoGrupo.addItem(grupo);
        }
    }

    private void rellenarCBConciertoCancion(){
        view.cbConciertoCancion.removeAllItems();
        ArrayList<Cancion> canciones = model.getListaCanciones();
        for (Cancion cancion : canciones) {
            view.cbConciertoCancion.addItem(cancion);
        }
    }*/
    private void guardarConcierto() {
        concierto = new Concierto();
        concierto.setNombre(view.tfconciertonombre.getText());
        concierto.setFecha(view.dateconciertofecha.getDate());
        concierto.setPrecio(Float.parseFloat(view.floatconciertoprecio.getText()));
        listaConciertos.add(concierto);
        model.guardarHibernate(concierto);

    }

    private void guardarGira() {
        Concierto concierto = (Concierto) view.cbGiraConciertos.getSelectedItem();
        concierto.setGrupo(view.JlistaGrupos.getSelectedValuesList());
        model.modificarHibernate(concierto);

    }



    private void modificarGrupo(Grupo grupo) {
        grupo.setNombre(view.tfgruponombre.getText());
        grupo.setGenero(view.tfgrupogenero.getText());
        grupo.setFecha_inicio(view.dategrupofinicio.getDate());
        grupo.setFecha_fin(view.dategrupoffin.getDate());
        grupo.setNacionalidad(view.tfgruponacionalidad.getText());

        listaGrupos.add(grupo);
        model.modificarHibernate(grupo);
    }

    private void modificarDisco(Disco disco) {
        disco.setTitulo(view.tfdiscotitulo.getText());
        disco.setDuracion(Float.parseFloat(view.floatdiscoduracion.getText()));
        disco.setProductora(view.tfdiscoproductora.getText());
        disco.setNum_canciones(Integer.parseInt(view.intdisconumcanciones.getText()));
        disco.setPrecio(Float.parseFloat(view.floatdiscoprecio.getText()));
        disco.setNombre_grupo(view.cbdiscoGrupo.getSelectedItem().toString());
        disco.setGrupo((Grupo) view.cbdiscoGrupo.getSelectedItem());
        listaDiscos.add(disco);
        model.modificarHibernate(disco);
    }

    private void modificarCancion(Cancion cancion) {
        cancion.setTitulo(view.tfcanciontitulo.getText());
        cancion.setDuracion(Float.parseFloat(view.floatcancionduracion.getText()));
        cancion.setTrack(Integer.parseInt(view.intcanciontrack.getText()));
        cancion.setPrecio(Float.parseFloat(view.floatcancionprecio.getText()));
        cancion.setGenero(view.tfcanciongenero.getText());
        cancion.setTitulo_disco(view.cbcancionDisco.getSelectedItem().toString());
        cancion.setDisco((Disco) view.cbcancionDisco.getSelectedItem());
        listaCanciones.add(cancion);
        model.modificarHibernate(cancion);
    }

    private void modificarConcierto(Concierto concierto) {
        concierto.setNombre(view.tfconciertonombre.getText());
        concierto.setFecha(view.dateconciertofecha.getDate());
        concierto.setPrecio(Float.parseFloat(view.floatconciertoprecio.getText()));
        listaConciertos.add(concierto);
        model.modificarHibernate(concierto);
    }

    private void rellenarCombodiscoGrupo() {
        view.cbdiscoGrupo.removeAllItems();
        ArrayList<Grupo> grupos = model.getListaGrupos();
        for (Grupo grupo : grupos) {
            view.cbdiscoGrupo.addItem(grupo);
        }
    }

    private void rellenarCombocancionDisco() {
        view.cbcancionDisco.removeAllItems();
        ArrayList<Disco> discos = model.getListaDiscos();
        for (Disco disco : discos) {
            view.cbcancionDisco.addItem(disco);
        }
    }




   /* @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {
        if(e.getSource()==view.tfgrupobuscar){
            if(e.getKeyCode()==KeyEvent.VK_ENTER){
                if(view.tfgrupobuscar.getText().equalsIgnoreCase(" ")){
                    listarTablaGrupos();
                    return;
                }else{
                    List<Grupo> listaG =  model.getBuscarGrupo(view.tfgrupobuscar.getText());
                    dlmGrupo.removeAllElements();
                    for(Grupo grupo : listaG){
                        dlmGrupo.addElement(grupo);
                    }
                    listarTablaGrupos();
                }


            }
        }
    }*/

}

