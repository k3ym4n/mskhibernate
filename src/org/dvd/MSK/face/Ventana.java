package org.dvd.MSK.face;

import com.toedter.calendar.JDateChooser;

import javax.swing.*;

/**
 * Created by k3ym4n on 02/03/2016.
 */
public class Ventana {
    public JTabbedPane PanelPrincipal;
    public JPanel TODO;
    public JList JlistaDcanciones;
    public JTable tablaDcanciones;
    public JComboBox cbcancionDisco;
    public JTextField tfcancionbuscar;
    public JList JlistaDgrupos;
    public JTable tablaDgrupos;
    public JList JlistaDdiscos;
    public JTable tablaDdiscos;
    public JComboBox cbdiscoGrupo;
    public JTextField tfgrupobuscar;
    public JTextField tfbuscardisco;
    public JTextField tfgruponombre;
    public JTextField tfgrupogenero;
    public JTextField tfgruponacionalidad;
    public JTextField tfdiscotitulo;
    public JTextField floatdiscoduracion;
    public JTextField tfdiscoproductora;
    public JTextField floatdiscoprecio;
    public JTextField intdisconumcanciones;
    public JTextField tfcanciontitulo;
    public JTextField floatcancionduracion;
    public JTextField intcanciontrack;
    public JTextField floatcancionprecio;
    public JTextField tfcanciongenero;
    public JDateChooser dategrupofinicio;
    public JDateChooser dategrupoffin;

    public Botonera botonera;
    public JTextField floatconciertoprecio;
    public JTable tablaGira;
    public JComboBox cbconciertoGrupo;
    public JComboBox cbconcierto;
    public JTable tablaConciertoGrupos;
    public JTable tablaConciertos;

    public JTextField tfconciertonombre;
    public JDateChooser dateconciertofecha;
    public JComboBox cbGiraGrupos;
    public JComboBox cbGiraConciertos;
    public JList JlistaGira;
    public JButton btguardarGira;
    public JList JlistaGrupos;
    public JList JlistaConciertos;
    public JTextField tfconciertobuscar;
    public JButton btbuscarGrupo;
    public JButton btbuscarDisco;
    public JButton btbuscarCancion;
    public JButton btbuscarConcierto;
    public JComboBox cbConciertoGrupo;
    public JComboBox cbConciertoCancion;
    public JTextField tfrecopproductora;
    public JList JlistaDrecopilatorios;
    public JButton btanadirgrupocancion;
    public JButton bteliminargrupocancion;
    public JTable tablaDrecopilatorios;
    public JDateChooser daterecopfecha;

    public Ventana() {
        JFrame frame = new JFrame("Ventana");
        frame.setContentPane(TODO);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


    public Botonera getBotonera() {
        return botonera;
    }

    public void setBotonera(Botonera botonera) {
        this.botonera = botonera;
    }
}
