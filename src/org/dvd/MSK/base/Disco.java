package org.dvd.MSK.base;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by k3ym4n on 02/03/2016.
 */
@Entity
@Table(name = "Disco")
public class Disco {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "titulo")
    private String titulo;
    @Column(name = "duracion")
    private float duracion;
    @Column(name = "productora")
    private String productora;
    @Column(name = "num_canciones")
    private int num_canciones;
    @Column(name = "precio")
    private float precio;
    @Column(name = "nombre_grupo")
    private String nombre_grupo;

    @ManyToOne
    @JoinColumn(name = "id_grupo")
    Grupo grupo;

    @OneToMany(mappedBy = "disco",cascade = CascadeType.ALL)
    List<Cancion> cancion;

    public Disco(){
        cancion = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public float getDuracion() {
        return duracion;
    }

    public void setDuracion(float duracion) {
        this.duracion = duracion;
    }

    public String getProductora() {
        return productora;
    }

    public void setProductora(String productora) {
        this.productora = productora;
    }

    public int getNum_canciones() {
        return num_canciones;
    }

    public void setNum_canciones(int num_canciones) {
        this.num_canciones = num_canciones;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public String getNombre_grupo() {
        return nombre_grupo;
    }

    public void setNombre_grupo(String nombre_grupo) {
        this.nombre_grupo = nombre_grupo;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public List<Cancion> getCancion() {
        return cancion;
    }

    public void setCancion(List<Cancion> cancion) {
        this.cancion = cancion;
    }

    @Override
    public String toString() {
        return titulo ;
    }
}
