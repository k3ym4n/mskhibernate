package org.dvd.MSK;

import org.dvd.MSK.face.Ventana;
import org.dvd.MSK.gui.VentanaController;
import org.dvd.MSK.gui.VentanaModel;

/**
 * Created by k3ym4n on 01/03/2016.
 */
public class Principal {

    public static void main(String arg[]){

        Ventana view = new Ventana();
        VentanaModel model = new VentanaModel();
        VentanaController controller = new VentanaController(view,model,view.getBotonera());
    }


}
