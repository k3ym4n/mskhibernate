package org.dvd.MSK.base;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by k3ym4n on 02/03/2016.
 */
@Entity
@Table(name = "Cancion")
public class Cancion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "titulo")
    private String titulo;
    @Column(name = "duracion")
    private float duracion;
    @Column(name = "track")
    private int track;
    @Column(name = "precio")
    private float precio;
    @Column(name = "genero")
    private String genero;
    @Column(name = "titulo_disco")
    private String titulo_disco;

    @ManyToOne
    @JoinColumn(name = "id_disco")
    Disco disco;

    @OneToMany(mappedBy = "cancion" ,cascade = CascadeType.ALL)
    List<Recopilatorio> recopilatorio;

    @ManyToMany(cascade = CascadeType.DETACH,mappedBy = "cancion")
    List<Concierto> concierto;

    public Cancion(){
      recopilatorio = new ArrayList<>();
        concierto = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public float getDuracion() {
        return duracion;
    }

    public void setDuracion(float duracion) {
        this.duracion = duracion;
    }

    public int getTrack() {
        return track;
    }

    public void setTrack(int track) {
        this.track = track;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getTitulo_disco() {
        return titulo_disco;
    }

    public void setTitulo_disco(String titulo_disco) {
        this.titulo_disco = titulo_disco;
    }

    public Disco getDisco() {
        return disco;
    }

    public void setDisco(Disco disco) {
        this.disco = disco;
    }

    public List<Recopilatorio> getRecopilatorio() {
        return recopilatorio;
    }

    public void setRecopilatorio(List<Recopilatorio> recopilatorio) {
        this.recopilatorio = recopilatorio;
    }

    public List<Concierto> getConcierto() {
        return concierto;
    }

    public void setConcierto(List<Concierto> concierto) {
        this.concierto = concierto;
    }

    @Override
    public String toString() {
        return titulo;
    }
}
